# AI Project CNN
This project using convolutional neural networks to recognize alphabetical letters.

## Requirements

* [Anaconda](https://www.anaconda.com/)

## Install

First enter the website of Anaconda and download the software. Run the application, and run Jupyter notebook. Then download this project and open the file Untitled33.ipynb in Jupyter notebook. Now, run the following command

```
pip install numpy
pip install matplotlib
pip install opencv
pip install tensorflow
```

Now press the double arrow button which says "restart the kernel, then re-run the whole notebook (with dialog)" in order to train the neural network and see the result.

## Contributing
@joelfredin

